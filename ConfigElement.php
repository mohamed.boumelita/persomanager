<?php


class ConfigElement
{
    private $key;
    private $value;

    function __construct($key, $value)
    {
        $this->key = $key;
        $this->value = str_replace("\n", "", $value);
    }

    public function write()
    {
        print "$this->key : $this->value";
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getKey()
    {
        return $this->key;
    }
};
?>