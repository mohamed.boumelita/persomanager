<?php
/**
 * Created by PhpStorm.
 * User: stephane
 * Date: 23/03/19
 * Time: 11:18
 */
include("utils.php");

class CsvFile
{
    private $header;
    private $rows;

    function __construct($header)
    {
        $this->header = string2row($header);
        $this->sortIndex = 0;
    }

    function addRow($rowData)
    {
        $this->rows[] = string2row($rowData);
    }

    function writeRow($row)
    {
        $line = '';
        foreach ($row as $element) {
            $line .= $element. " ";
        }
        print "\n$line";
    }

    function write()
    {
        $this->writeRow($this->header);
        foreach ($this->rows as $element) {
            $this->writeRow($element);
        }
    }

    function compareBysort($a, $b) {
        return strcmp($a[$this->sortIndex], $b[$this->sortIndex]);
    }

    function sort($column)
    {
        $sortIndex = array_search($column, $this->header);

        usort($this->rows, function($a, $b) use ($sortIndex) {
            //$myExtraArgument is available in this scope
            //perform sorting, return -1, 0, 1
            return strcmp($a[$sortIndex], $b[$sortIndex]);
        });
    }
}
?>
